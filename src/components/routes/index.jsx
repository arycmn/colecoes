import { Switch, Route } from "react-router-dom";
import { useState } from "react";
//pages
import Home from "../../pages/home";
import RickAndMorty from "../../pages/rick-and-morty";
import Pokemon from "../../pages/pokemon";
import Favorites from "../../pages/favorites";

const Routes = ({
  favoritesRickAndMorty,
  setFavoritesRickAndMorty,
  favoritesPokemon,
  setFavoritesPokemon,
}) => {
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>

      <Route path="/rick-and-morty">
        <RickAndMorty
          favoritesRickAndMorty={favoritesRickAndMorty}
          setFavoritesRickAndMorty={setFavoritesRickAndMorty}
        />
      </Route>

      <Route path="/pokemon">
        <Pokemon
          favoritesPokemon={favoritesPokemon}
          setFavoritesPokemon={setFavoritesPokemon}
        />
      </Route>
      <Route path="/favorites">
        <Favorites
          favoritesPokemon={favoritesPokemon}
          favoritesRickAndMorty={favoritesRickAndMorty}
          setFavoritesRickAndMorty={setFavoritesRickAndMorty}
          setFavoritesPokemon={setFavoritesPokemon}
        />
      </Route>
    </Switch>
  );
};

export default Routes;
