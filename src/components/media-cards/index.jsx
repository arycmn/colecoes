import React from "react";
import { makeStyles } from "@material-ui/core/styles";

import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

import { CardStyled, Character } from "../../styles/index";
const useStyles = makeStyles({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 320,
  },
});

export default function MediaCard({
  name,
  status,
  species,
  gender,
  image,
  id,
  setFavoritesRickAndMorty,
  favoritesRickAndMorty,
  setFavoritesPokemon,
  favoritesPokemon,
  franquia,
  favorite,
}) {
  const classes = useStyles();
  const favoriteAction = () => {
    franquia === "rickAndMorty"
      ? setFavoritesRickAndMorty([
          ...favoritesRickAndMorty,
          {
            name: name,
            image: image,
            status: status,
            species: species,
            gender: gender,
            franquia: franquia,
          },
        ])
      : setFavoritesPokemon([
          ...favoritesPokemon,
          { name: name, image: image, id: id, franquia: franquia },
        ]);
  };
  const removeFavorite = () => {
    franquia === "rickAndMorty"
      ? setFavoritesRickAndMorty(
          favoritesRickAndMorty.map((character) => character.name !== name)
        )
      : setFavoritesPokemon(
          favoritesPokemon.map((character) => character.name !== name)
        );
  };

  return (
    <CardStyled className={classes.root}>
      <CardActionArea>
        <CardMedia className={classes.media} image={image} title={name} />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {name}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {status && `status: ${status},`} {species && `specie: ${species},`}{" "}
            {gender && `gender: ${gender}`} {id && `Number: ${id}`}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button
          size="small"
          color="primary"
          onClick={favorite ? removeFavorite : favoriteAction}
        >
          Favorite
        </Button>
      </CardActions>
    </CardStyled>
  );
}
