import MediaCard from "../../components/media-cards";
import { useState, useEffect } from "react";
import Axios from "axios";
import { Link } from "react-router-dom";

//components
import {
  CharacterContainer,
  PageContent,
  ButtonsContainer,
  Character,
} from "../../styles";
import Button from "@material-ui/core/Button";

const Pokemon = ({ favoritesPokemon, setFavoritesPokemon }) => {
  const [numberPokemon, setNumberPokemon] = useState(2);
  const [url, setUrl] = useState("https://pokeapi.co/api/v2/pokemon/1");
  const urlBase = "https://pokeapi.co/api/v2/pokemon/";
  const [arrayPokemon, setArrayPokemon] = useState([]);

  useEffect(() => {
    Axios.get(url)
      .then(function (response) {
        setArrayPokemon([...arrayPokemon, { ...response.data }]);
        if (numberPokemon < 151) {
          setNumberPokemon(numberPokemon + 1);
        }
        setUrl(`${urlBase}${numberPokemon}`);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [url]);

  return (
    <PageContent>
      <Link to="/">Voltar</Link>
      <h1>Pokémon Characters</h1>
      <ButtonsContainer></ButtonsContainer>
      <CharacterContainer>
        {arrayPokemon.map((character, index) => (
          <Character key={index}>
            <MediaCard
              franquia="pokemon"
              setFavoritesPokemon={setFavoritesPokemon}
              favoritesPokemon={favoritesPokemon}
              name={character.name}
              id={character.id}
              image={character.sprites.other["official-artwork"].front_default}
              favorite={false}
            />
          </Character>
        ))}
        <div></div>
      </CharacterContainer>
    </PageContent>
  );
};

export default Pokemon;
