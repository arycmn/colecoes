import MediaCard from "../../components/media-cards";
import { useState, useEffect } from "react";
import Axios from "axios";
import { Link } from "react-router-dom";

//components
import {
  CharacterContainer,
  PageContent,
  ButtonsContainer,
  Character,
} from "../../styles";
import Button from "@material-ui/core/Button";

const RickAndMorty = ({ favoritesRickAndMorty, setFavoritesRickAndMorty }) => {
  const [url, SetUrl] = useState(
    "https://rickandmortyapi.com/api/character/?page=1"
  );
  const [characters, setCharacters] = useState([]);
  const [prev, SetPrev] = useState();
  const [next, SetNext] = useState();

  useEffect(() => {
    Axios.get(url).then(function (response) {
      setCharacters([...response.data.results]);
      SetPrev(response.data.info.prev);
      SetNext(response.data.info.next);
    });
  }, [url]);

  return (
    <PageContent>
      <Link to="/">Voltar</Link>
      <h1>Rick and Morty Characters</h1>
      <ButtonsContainer>
        {prev !== null && (
          <Button
            onClick={() => SetUrl(prev)}
            variant="contained"
            color="primary"
          >
            Prev
          </Button>
        )}
        {next !== null && (
          <Button
            onClick={() => SetUrl(next)}
            variant="contained"
            color="primary"
          >
            Next
          </Button>
        )}
      </ButtonsContainer>
      <CharacterContainer>
        {characters.map((character, index) => (
          <Character key={index}>
            <MediaCard
              favoritesRickAndMorty={favoritesRickAndMorty}
              setFavoritesRickAndMorty={setFavoritesRickAndMorty}
              name={character.name}
              status={character.status}
              species={character.species}
              gender={character.gender}
              image={character.image}
              franquia="rickAndMorty"
              favorite={false}
            />
          </Character>
        ))}
        <div></div>
      </CharacterContainer>
    </PageContent>
  );
};

export default RickAndMorty;
