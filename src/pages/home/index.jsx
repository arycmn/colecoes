import { Link } from "react-router-dom";

const Home = () => {
  return (
    <>
      <ul>
        <li>
          <Link to="/rick-and-morty">Rick and Morty</Link>
        </li>
        <li>
          <Link to="/Pokemon">Pokémon</Link>
        </li>
        <li>
          <Link to="/favorites">Favorites</Link>
        </li>
      </ul>
    </>
  );
};

export default Home;
