import MediaCard from "../../components/media-cards";
import { useEffect, useState } from "react";

import {
  CharacterContainer,
  PageContent,
  ButtonsContainer,
  Character,
} from "../../styles";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";

const Favorites = ({
  favoritesPokemon,
  favoritesRickAndMorty,
  setFavoritesRickAndMorty,
  setFavoritesPokemon,
}) => {
  const [franquia, setFranquia] = useState("Pokémon");
  const [updatedFavoritesPokemon, setUpdatedFavoritesPokemon] = useState([]);
  const [
    updatedFavoritesRickAndMorty,
    setUpdatedFavoritesRickAndMorty,
  ] = useState([]);

  const ChangeFranquia = () => {
    franquia === "Pokémon"
      ? setFranquia("Rick and Morty")
      : setFranquia("Pokémon");
  };

  useEffect(() => {
    setUpdatedFavoritesPokemon([...favoritesPokemon]);
    setUpdatedFavoritesRickAndMorty([...favoritesRickAndMorty]);
  }, [favoritesPokemon, favoritesRickAndMorty]);

  console.log(favoritesPokemon, favoritesRickAndMorty);
  return (
    <PageContent>
      <Link to="/">Voltar</Link>
      <h1>Favorites</h1>
      <ButtonsContainer>
        <Button onClick={ChangeFranquia}>{franquia}</Button>
      </ButtonsContainer>
      <CharacterContainer>
        {franquia === "Pokémon"
          ? updatedFavoritesPokemon.map((character, index) => (
              <Character key={index}>
                <MediaCard
                  franquia="pokemon"
                  favoritesPokemon={favoritesPokemon}
                  setFavoritesPokemon={setFavoritesPokemon}
                  name={character.name}
                  id={character.id}
                  image={character.image}
                  favorite={true}
                />
              </Character>
            ))
          : updatedFavoritesRickAndMorty.map((character, index) => (
              <Character key={index}>
                <MediaCard
                  favoritesRickAndMorty={favoritesRickAndMorty}
                  setFavoritesRickAndMorty={setFavoritesRickAndMorty}
                  name={character.name}
                  status={character.status}
                  species={character.species}
                  gender={character.gender}
                  image={character.image}
                  franquia="rickAndMorty"
                  favorite={true}
                />
              </Character>
            ))}
      </CharacterContainer>
    </PageContent>
  );
};

export default Favorites;
