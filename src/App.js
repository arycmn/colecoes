import { useState } from "react";

//components
import Routes from "./components/routes";

function App() {
  const [favoritesRickAndMorty, setFavoritesRickAndMorty] = useState([]);
  const [favoritesPokemon, setFavoritesPokemon] = useState([]);
  return (
    <div>
      <Routes
        favoritesRickAndMorty={favoritesRickAndMorty}
        setFavoritesRickAndMorty={setFavoritesRickAndMorty}
        favoritesPokemon={favoritesPokemon}
        setFavoritesPokemon={setFavoritesPokemon}
      />
    </div>
  );
}

export default App;
