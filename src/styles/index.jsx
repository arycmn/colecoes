import styled from "styled-components";
import Card from "@material-ui/core/Card";

export const CharacterContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  background-color: gray;

  div {
    margin-top: 15px;
  }
`;

export const PageContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  background-color: darkgray;
  h1 {
    text-align: center;
  }
`;

export const ButtonsContainer = styled.div`
  display: flex;
  justify-content: center;
`;

export const Character = styled.div`
  width: 400px;
  height: 550px;
`;

export const CardStyled = styled(Card)`
  width: 350px;
  height: 500px;
`;
